// ##################################################################
// # Do NOT edit any of the lines before the "// StartStudentCode"  #
// # line or after the "// EndStudentCode line. Do not remove those #
// # two lines.                                                     #
// #                                                                #
// # If you do edit any of the other code, your submission will     #
// # probably not work.                                             #
// ##################################################################

// StartStudentCode
function find(needle, haystack) {
    var left = 0;
    var right = haystack.length - 1;
    while(left <= right) {
        var middle = Math.floor(left + (right - left) / 2);
        if(needle == haystack[middle].snr) {
            return haystack[middle];
        } else if(haystack[middle] < needle) {
            left = middle + 1;
        } else {
            right = middle - 1;
        }
    }
    return null;
}
// EndStudentCode

var assert = require('assert');
describe('Question 3', function() {
    it('test', function() {
        var students = [{name: 'Mark', snr: 9947832}, {name: 'Dave', snr: 483373}, {name: 'Chris', snr: 6482724}];
        assert.deepEqual(find(483373, students), {name: 'Dave', snr: 483373});
    });
});
