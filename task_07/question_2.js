// ##################################################################
// # Do NOT edit any of the lines before the "// StartStudentCode"  #
// # line or after the "// EndStudentCode line. Do not remove those #
// # two lines.                                                     #
// #                                                                #
// # If you do edit any of the other code, your submission will     #
// # probably not work.                                             #
// ##################################################################

// StartStudentCode
function merge(a, b) {
    if(a.length > 0 && b.length > 0) {
        if(a[0] >= b[0]) {
            var result = [a[0]];
            var sorted = merge(a.slice(1), b);
            result = result.concat(sorted);
            return result;
        } else {
            var result = [b[0]];
            var sorted = merge(a, b.slice(1));
            result = result.concat(sorted)
            return result;
        }
    } else if(a.length > 0 && b.length == 0) {
        return a;
    } else if(a.length == 0 && b.length > 0) {
        return b;
    }
}
function reverse_merge(list) {
    if(list.length > 1) {
        var split_point = list.length / 2;
        var part1 = list.slice(0, split_point);
        var part2 = list.slice(split_point);
        var sorted1 = reverse_merge(part1);
        var sorted2 = reverse_merge(part2);
        return merge(sorted1, sorted2);
    } else {
        return list;
    }
}
// EndStudentCode

var assert = require('assert');
describe('Question 2', function() {
    it('test', function() {
        var items = [73, 32, 13, 24, 8, 66];
        var sorted = reverse_merge(items.slice());
        assert.deepEqual(sorted, [73, 66, 32, 24, 13, 8]);
        items = [43, 87, 12];
        sorted = reverse_merge(items.slice());
        assert.deepEqual(sorted, [87, 43, 12]);
    });
});
