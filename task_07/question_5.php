<?php
// ##################################################################
// # Do NOT edit any of the lines before the "// StartStudentCode"  #
// # line or after the "// EndStudentCode line. Do not remove those #
// # two lines.                                                     #
// #                                                                #
// # If you do edit any of the other code, your submission will     #
// # probably not work.                                             #
// ##################################################################

// StartStudentCode
function merge($a, $b) {
    if(count($a) > 0 && count($b) > 0) {
        if($a[0]->snr <= $b[0]->snr) {
            return array_merge([$a[0]], merge(array_slice($a, 1), $b));
        } else {
            return array_merge([$b[0]], merge($a, array_slice($b, 1)));
        }
    } else if(count($a) > 0) {
        return $a;
    } else if(count($b) > 0) {
        return $b;
    }
}

function student_sort($items) {
    if(count($items) > 1) {
        $split_point = count($items) / 2;
        $part1 = array_slice($items, 0, $split_point);
        $part2 = array_slice($items, $split_point);
        $sorted1 = student_sort($part1);
        $sorted2 = student_sort($part2);
        return merge($sorted1, $sorted2);
    } else {
        return $items;
    }
}
// EndStudentCode

class Student {
    function __construct($name, $snr) {
        $this->name = $name;
        $this->snr = $snr;
    }
}

class Question5Test extends PHPUnit_Framework_TestCase {
    public function test() {
        $mark = new Student('Mark', 9947832);
        $dave = new Student('Dave', 483373);
        $chris = new Student('Chris', 6482724);
        $students = [$mark, $dave, $chris];
        $sorted = student_sort($students);
        $this->assertEquals([$dave, $chris, $mark], $sorted);
    }
}
