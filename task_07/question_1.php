<?php
// ##################################################################
// # Do NOT edit any of the lines before the "// StartStudentCode"  #
// # line or after the "// EndStudentCode line. Do not remove those #
// # two lines.                                                     #
// #                                                                #
// # If you do edit any of the other code, your submission will     #
// # probably not work.                                             #
// ##################################################################

// StartStudentCode
function reverse_insertion($list) {
    for($idx = 1; $idx < count($list); $idx++) {
        $idx2 = $idx;
        while($idx2 > 0 && $list[$idx2 - 1] < $list[$idx2]) {
            $tmp = $list[$idx2 - 1];
            $list[$idx2 - 1] = $list[$idx2];
            $list[$idx2] = $tmp;
            $idx2 = $idx2 - 1;
        }
    }
    return $list;
}
// EndStudentCode

class Question1Test extends PHPUnit_Framework_TestCase {
    public function test() {
        $items = [73, 32, 13, 24, 8, 66];
        $sorted = reverse_insertion($items);
        $this->assertEquals([73, 66, 32, 24, 13, 8], $sorted);
        $items = [43, 87, 12];
        $sorted = reverse_insertion($items);
        $this->assertEquals([87, 43, 12], $sorted);
    }
}
