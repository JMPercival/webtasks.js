<?php
// ##################################################################
// # Do NOT edit any of the lines before the "// StartStudentCode"  #
// # line or after the "// EndStudentCode line. Do not remove those #
// # two lines.                                                     #
// #                                                                #
// # If you do edit any of the other code, your submission will     #
// # probably not work.                                             #
// ##################################################################

// StartStudentCode
function minimum($a, $b) {
    if(count($a) == 0) {
        return [];
    } else {
        $result = [];
        if($a[0] <= $b[0]) {
            $result[] = $a[0];
        } else {
            $result[] = $b[0];
        }
        $recursive_result = minimum(array_slice($a, 1), array_slice($b, 1));
        $result = array_merge($result, $recursive_result);
        return $result;
    }
}
// EndStudentCode

class Question4Test extends PHPUnit_Framework_TestCase {
    public function test() {
        $this->assertEquals([], minimum([], []));
        $a = [37, 4, 42, 62, 82];
        $b = [92, 8, 42, 34, 102];
        $this->assertEquals([37, 4, 42, 34, 82], minimum($a, $b));
    }
}
