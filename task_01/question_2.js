// ##################################################################
// # Do NOT edit any of the lines before the "// StartStudentCode"  #
// # line or after the "// EndStudentCode line. Do not remove those #
// # two lines.                                                     #
// #                                                                #
// # If you do edit any of the other code, your submission will     #
// # probably not work.                                             #
// ##################################################################

var assert = require('assert');
describe('Question 2', function() {
    it('test', function() {
        // StartStudentCode
        var a = 4;
        var b = 2;
        var c = 6;
        // EndStudentCode
        assert.equal(a, 4);
        assert.equal(b, 2);
        assert.equal(c, 6);
    });
});
