<?php
// ##################################################################
// # Do NOT edit any of the lines before the "// StartStudentCode"  #
// # line or after the "// EndStudentCode line. Do not remove those #
// # two lines.                                                     #
// #                                                                #
// # If you do edit any of the other code, your submission will     #
// # probably not work.                                             #
// ##################################################################

class Question3Test extends PHPUnit_Framework_TestCase {
    public function test() {
        $name = 'Fred';
        // StartStudentCode
        $welcome = 'Welcome to University, ' . $name;
        // EndStudentCode
        $this->assertEquals('Welcome to University, Fred', $welcome);
    }
}
