<?php
// ##################################################################
// # Do NOT edit any of the lines before the "// StartStudentCode"  #
// # line or after the "// EndStudentCode line. Do not remove those #
// # two lines.                                                     #
// #                                                                #
// # If you do edit any of the other code, your submission will     #
// # probably not work.                                             #
// ##################################################################

class Question5Test extends PHPUnit_Framework_TestCase {
    public function test() {
        $random = 14;
        // StartStudentCode
        $remainder = $random % 5;

        // EndStudentCode
        $this->assertEquals(4, $remainder);
    }
}
