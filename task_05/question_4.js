// ##################################################################
// # Do NOT edit any of the lines before the "// StartStudentCode"  #
// # line or after the "// EndStudentCode line. Do not remove those #
// # two lines.                                                     #
// #                                                                #
// # If you do edit any of the other code, your submission will     #
// # probably not work.                                             #
// ##################################################################

// StartStudentCode
function Automobile(make, model, wheels) {
    this.make = make;
    this.model = model;
    this.wheels = wheels;
}

Automobile.prototype.summary = function() {
    return this.make + ' ' + this.model + ' (' + this.wheels + ' wheels)';
}

function Car(make, model) {
    Automobile.call(this, make, model, 4);
}
Car.prototype = new Automobile();

function Bike(make, model) {
    Automobile.call(this, make, model, 2);
}
Bike.prototype = new Automobile();
// EndStudentCode

var assert = require('assert');
describe('Question 4', function() {
    it('test', function() {
        var bmw = new Car('BMW', 'Z2');
        assert.equal(bmw.make, 'BMW');
        assert.equal(bmw.model, 'Z2');
        assert.equal(bmw.wheels, 4);
        assert.equal(bmw.summary(), 'BMW Z2 (4 wheels)');
        var ktm = new Bike('KTM', '1050 Adventure');
        assert.equal(ktm.make, 'KTM');
        assert.equal(ktm.model, '1050 Adventure');
        assert.equal(ktm.wheels, 2);
        assert.equal(ktm.summary(), 'KTM 1050 Adventure (2 wheels)');
    });
});
