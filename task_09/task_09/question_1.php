<?php
// ##################################################################
// # Do NOT edit any of the lines before the "// StartStudentCode"  #
// # line or after the "// EndStudentCode line. Do not remove those #
// # two lines.                                                     #
// #                                                                #
// # If you do edit any of the other code, your submission will     #
// # probably not work.                                             #
// ##################################################################

/**
 * Node class represents a single node in a tree.
 *
 * It has three properties:
 * * id - Sort-order identifier
 * * label - The label
 * * children - List of children
 */
 class Node {
     public function __construct($id, $label, $children) {
         $this->id = $id;
         $this->label = $label;
         $this->children = $children;
     }
 }


 class Question1Test extends PHPUnit_Framework_TestCase {
     public function test() {
         // StartStudentCode
         $tree = new Node(1, 'One', [new Node(2, 'Two', [new Node(3, 'Three', []),
                                                         new Node(4, 'Four', [])]),
                                     new Node(5, 'Five', [new Node(6, 'Six', [new Node(7, 'Seven', [])])]),
                                     new Node(8, 'Eight', [new Node(9, 'Nine', []),
                                                           new Node(10, 'Ten', []),
                                                           new Node(11, 'Eleven', [])])]);
        // EndStudentCode
        $this->assertEquals(1, $tree->id);
        $this->assertEquals('One', $tree->label);
        $this->assertEquals(3, count($tree->children));
        $this->assertEquals(2, $tree->children[0]->id);
        $this->assertEquals(5, $tree->children[1]->id);
        $this->assertEquals(1, count($tree->children[1]->children));
        $this->assertEquals(1, count($tree->children[1]->children[0]->children));
        $this->assertEquals(8, $tree->children[2]->id);
        $this->assertEquals(3, count($tree->children[2]->children));
    }
}
