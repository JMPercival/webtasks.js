<?php

// ##################################################################
// # Do NOT edit any of the lines before the "// StartStudentCode"  #
// # line or after the "// EndStudentCode line. Do not remove those #
// # two lines.                                                     #
// #                                                                #
// # If you do edit any of the other code, your submission will     #
// # probably not work.                                             #
// ##################################################################

// StartStudentCode
function odd_number($n) {
    if($n == 0) {
        return 1;
    } else {
        return odd_number($n - 1) + 2;
    }
}
// EndStudentCode

class Question2Test extends PHPUnit_Framework_TestCase {
    public function test() {
        $this->assertEquals(3, odd_number(1));
        $this->assertEquals(9, odd_number(4));
        for($idx = 0; $idx < 10; $idx++) {
            $n = rand(1, 100);
            $this->assertEquals($n * 2 + 1, odd_number($n));
        }
    }
}
