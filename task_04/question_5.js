// ##################################################################
// # Do NOT edit any of the lines before the "// StartStudentCode"  #
// # line or after the "// EndStudentCode line. Do not remove those #
// # two lines.                                                     #
// #                                                                #
// # If you do edit any of the other code, your submission will     #
// # probably not work.                                             #
// ##################################################################

// StartStudentCode
function count_whitespace(str) {
    if(str.length === 0) {
        return 0;
    } else {
        if(str.charAt(0) === ' ') {
            return count_whitespace(str.slice(1)) + 1;
        } else {
            return count_whitespace(str.slice(1));
        }
    }
}
// EndStudentCode

var assert = require('assert');
describe('Question 5', function() {
    it('test', function() {
        assert.equal(count_whitespace('This string has five whitespace characters.'), 5);
        assert.equal(count_whitespace('Nothing'), 0);
        assert.equal(count_whitespace('Just one'), 1);
    });
});
