// ##################################################################
// # Do NOT edit any of the lines before the "// StartStudentCode"  #
// # line or after the "// EndStudentCode line. Do not remove those #
// # two lines.                                                     #
// #                                                                #
// # If you do edit any of the other code, your submission will     #
// # probably not work.                                             #
// ##################################################################

// StartStudentCode
function str_reverse(str) {
    if(str.length === 0) {
        return str;
    } else {
        return str_reverse(str.slice(1)) + str.slice(0, 1);
    }
}
// EndStudentCode

var assert = require('assert');
describe('Question 3', function() {
    it('test', function() {
        assert.equal(str_reverse('foo'), 'oof');
        assert.equal(str_reverse('test'), 'tset');
        assert.equal(str_reverse('anna'), 'anna');
    });
});
