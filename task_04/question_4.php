<?php
// ##################################################################
// # Do NOT edit any of the lines before the "// StartStudentCode"  #
// # line or after the "// EndStudentCode line. Do not remove those #
// # two lines.                                                     #
// #                                                                #
// # If you do edit any of the other code, your submission will     #
// # probably not work.                                             #
// ##################################################################

// StartStudentCode
function str_reverse($str) {
    if(strlen($str) == 0) {
        return $str;
    } else {
        return str_reverse(substr($str, 1)) . substr($str, 0, 1);
    }
}
// EndStudentCode

class Question4Test extends PHPUnit_Framework_TestCase {
    public function test() {
        $this->assertEquals('oof', str_reverse('foo'));
        $this->assertEquals('tset', str_reverse('test'));
        $this->assertEquals('anna', str_reverse('anna'));
    }
}
