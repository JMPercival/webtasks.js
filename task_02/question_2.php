<?php
// ##################################################################
// # Do NOT edit any of the lines before the "// StartStudentCode"  #
// # line or after the "// EndStudentCode line. Do not remove those #
// # two lines.                                                     #
// #                                                                #
// # If you do edit any of the other code, your submission will     #
// # probably not work.                                             #
// ##################################################################

class Question2Test extends PHPUnit_Framework_TestCase {
    public function test() {
        $number = rand(1, 100);
        // StartStudentCode
      if($number % 2 == 0) {
          $result = $number / 2;
        } else {
          $result = $number * 2;
        }
        print($result);
        // EndStudentCode
        $this->assertEquals($number%2==0?$number/2:$number*2, $result);
    }
}
