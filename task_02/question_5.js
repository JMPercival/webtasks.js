// ##################################################################
// # Do NOT edit any of the lines before the "// StartStudentCode"  #
// # line or after the "// EndStudentCode line. Do not remove those #
// # two lines.                                                     #
// #                                                                #
// # If you do edit any of the other code, your submission will     #
// # probably not work.                                             #
// ##################################################################

var assert = require('assert');
describe('Question 5', function() {
    it('test', function() {
        var number = 3;
        // StartStudentCode
        var count = 0;
        while (number < 21297) {
          count = count + 1;
          number = number * 2;
         }
        // EndStudentCode
        assert.deepEqual(count, 13);
    });
});
