// ##################################################################
// # Do NOT edit any of the lines before the "// StartStudentCode"  #
// # line or after the "// EndStudentCode line. Do not remove those #
// # two lines.                                                     #
// #                                                                #
// # If you do edit any of the other code, your submission will     #
// # probably not work.                                             #
// ##################################################################

var assert = require('assert');
describe('Question 1', function() {
    it('test', function() {
      var data = {}
      data['A']  = '65';
      data['B']  = '66';
      data['C']  = '67';
        // StartStudentCode
        assert.equal(data['A'], 65);
        assert.equal(data['B'], 66);
        assert.equal(data['C'], 67);
      });
});

        // EndStudentCode
        assert.equal(data['A'], 65);
        assert.equal(data['B'], 66);
        assert.equal(data['C'], 67);
    });
});
