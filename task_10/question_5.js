// ##################################################################
// # Do NOT edit any of the lines before the "// StartStudentCode"  #
// # line or after the "// EndStudentCode line. Do not remove those #
// # two lines.                                                     #
// #                                                                #
// # If you do edit any of the other code, your submission will     #
// # probably not work.                                             #
// ##################################################################

// StartStudentCode
function common_length(a, b) {
    if(a.length > 0 && b.length > 0) {
        if(a.charAt(0) === b.charAt(0)) {
            return common_length(a.substr(1), b.substr(1)) + 1;
        } else {
            return 0;
        }
    } else {
        return 0;
    }
}
// EndStudentCode

var assert = require('assert');
describe('Question 5', function() {
    it('test', function() {
        assert.equal(common_length('Hello', 'Bye'), 0);
        assert.equal(common_length('Hello', 'Hello World'), 5);
        assert.equal(common_length('Heap', 'Hello'), 2);
        assert.equal(common_length('This is it', 'This is it'), 10);
    });
});
