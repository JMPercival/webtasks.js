// ##################################################################
// # Do NOT edit any of the lines before the "// StartStudentCode"  #
// # line or after the "// EndStudentCode line. Do not remove those #
// # two lines.                                                     #
// #                                                                #
// # If you do edit any of the other code, your submission will     #
// # probably not work.                                             #
// ##################################################################

var assert = require('assert');
describe('Question 3', function() {
    it('test', function() {
        var data = {}
        data['One'] = 1;
        data['Two'] = 2;
        delete data['One'];
        // StartStudentCode
           delete data['One'];
        // EndStudentCode
        assert.equal(data['One'], undefined);
        assert.equal(data['Two'], 2);
    });
});
