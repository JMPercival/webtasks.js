<?php
// ##################################################################
// # Do NOT edit any of the lines before the "// StartStudentCode"  #
// # line or after the "// EndStudentCode line. Do not remove those #
// # two lines.                                                     #
// #                                                                #
// # If you do edit any of the other code, your submission will     #
// # probably not work.                                             #
// ##################################################################

class Question4Test extends PHPUnit_Framework_TestCase {
    public function test() {
        $data = array();
        $data['One'] = 1;
        $data['Two'] = 2;

        // StartStudentCode
        unset($data['One']);
        // EndStudentCode
        $this->assertEquals(false, array_key_exists('One', $data));
        $this->assertEquals(2, $data['Two']);
    }
}
