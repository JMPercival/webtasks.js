<?php
// ##################################################################
// # Do NOT edit any of the lines before the "// StartStudentCode"  #
// # line or after the "// EndStudentCode line. Do not remove those #
// # two lines.                                                     #
// #                                                                #
// # If you do edit any of the other code, your submission will     #
// # probably not work.                                             #
// ##################################################################

class Question2Test extends PHPUnit_Framework_TestCase {
    public function test() {
      $data = array();

      $data['A'] = '65';
      $data['B'] = '66';
      $data['C'] = '67';

        // StartStudentCode
           $data = array('A' => 65, 'B' => 66, 'C' => 67);
        // EndStudentCode
        $this->assertEquals(65, $data['A']);
        $this->assertEquals(66, $data['B']);
        $this->assertEquals(67, $data['C']);
    }
}
